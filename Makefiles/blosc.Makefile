BLOSCTOP = $(APP)/bloscSrc

USR_CFLAGS += -DHAVE_ZSTD
USR_CFLAGS += -DHAVE_LZ4
USR_CFLAGS += -DHAVE_ZLIB
USR_CFLAGS += -DHAVE_SNAPPY


USR_INCLUDES += -I$(where_am_I)$(BLOSCTOP)/internal-complibs/zstd-1.3.0/

SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/entropy_common.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/error_private.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/fse_decompress.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/pool.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/threading.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/xxhash.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/common/zstd_common.c

SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/compress/fse_compress.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/compress/zstd_compress.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/compress/huf_compress.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/compress/zstdmt_compress.c

SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/decompress/huf_decompress.c
SOURCES += $(BLOSCTOP)/internal-complibs/zstd-1.3.0/decompress/zstd_decompress.c

SOURCES += $(BLOSCTOP)/internal-complibs/snappy-1.1.1/snappy.cc
SOURCES += $(BLOSCTOP)/internal-complibs/snappy-1.1.1/snappy-c.cc
SOURCES += $(BLOSCTOP)/internal-complibs/snappy-1.1.1/snappy-sinksource.cc
SOURCES += $(BLOSCTOP)/internal-complibs/snappy-1.1.1/snappy-stubs-internal.cc


SOURCES += $(BLOSCTOP)/internal-complibs/lz4-1.7.5/lz4.c
SOURCES += $(BLOSCTOP)/internal-complibs/lz4-1.7.5/lz4hc.c


HEADERS += $(BLOSCTOP)/blosc/blosc.h
HEADERS += $(BLOSCTOP)/blosc/blosc-export.h

SOURCES += $(BLOSCTOP)/blosc/blosc.c
SOURCES += $(BLOSCTOP)/blosc/blosclz.c
SOURCES += $(BLOSCTOP)/blosc/shuffle-generic.c
SOURCES += $(BLOSCTOP)/blosc/bitshuffle-generic.c
SOURCES += $(BLOSCTOP)/blosc/shuffle.c



ifeq ($(BLOSC_SSE2),YES)
SOURCES += $(BLOSCTOP)/blosc/shuffle-sse2.c
SOURCES += $(BLOSCTOP)/blosc/bitshuffle-sse2.c
endif

ifeq ($(BLOSC_AVX2),YES)
SOURCES += $(BLOSCTOP)/blosc/shuffle-avx2.c
SOURCES += $(BLOSCTOP)/blosc/bitshuffle-avx2.c
endif

