NEXUSTOP:=$(APP)/nexusSrc

HEADERS += $(NEXUSTOP)/$(OS_default)/napi.h
HEADERS += $(NEXUSTOP)/$(OS_default)/napiconfig.h
HEADERS += $(NEXUSTOP)/$(OS_Linux)/nxconfig.h

SOURCES += $(NEXUSTOP)/napi.c
SOURCES += $(NEXUSTOP)/napi5.c
SOURCES += $(NEXUSTOP)/napiu.c
SOURCES += $(NEXUSTOP)/nxdataset.c
SOURCES += $(NEXUSTOP)/nxio.c
SOURCES += $(NEXUSTOP)/nxstack.c
SOURCES += $(NEXUSTOP)/nxxml.c
SOURCES += $(NEXUSTOP)/stptok.c

USR_CFLAGS += -DHDF5 -D_FILE_OFFSET_BITS=64

# Travis/ubuntu 12.04 tweak: persuade the hdf5 library build to use API v18 over v16
#USR_CFLAGS += -DH5_NO_DEPRECATED_SYMBOLS -DH5Gopen_vers=2
