NETCDFTOP = $(APP)/netCDFSrc

USR_INCLUDES += -I$(where_am_I)$(NETCDFTOP)/inc
USR_INCLUDES += -I$(where_am_I)$(NETCDFTOP)/libsrc
USR_INCLUDES += -I$(where_am_I)$(NETCDFTOP)/libdispatch
USR_INCLUDES += -I$(where_am_I)$(NETCDFTOP)/$(OS_Linux)
USR_INCLUDES += -I$(where_am_I)$(NETCDFTOP)/$(OS_default)

USR_CFLAGS += -DHAVE_CONFIG_H

HEADERS += $(NETCDFTOP)/inc/netcdf.h

SOURCES += $(NETCDFTOP)/libsrc/v1hpg.c
SOURCES += $(NETCDFTOP)/libsrc/putget.c
SOURCES += $(NETCDFTOP)/libsrc/attr.c
SOURCES += $(NETCDFTOP)/libsrc/nc3dispatch.c
SOURCES += $(NETCDFTOP)/libsrc/nc3internal.c
SOURCES += $(NETCDFTOP)/libsrc/var.c
SOURCES += $(NETCDFTOP)/libsrc/dim.c
SOURCES += $(NETCDFTOP)/libsrc/ncx.c
SOURCES += $(NETCDFTOP)/libsrc/nc_hashmap.c
SOURCES += $(NETCDFTOP)/libsrc/lookup3.c
SOURCES += $(NETCDFTOP)/libsrc/ncio.c
SOURCES += $(NETCDFTOP)/libsrc/memio.c
SOURCES += $(NETCDFTOP)/libsrc/posixio.c

SOURCES += $(NETCDFTOP)/libdispatch/dparallel.c
SOURCES += $(NETCDFTOP)/libdispatch/dcopy.c
SOURCES += $(NETCDFTOP)/libdispatch/dfile.c
SOURCES += $(NETCDFTOP)/libdispatch/ddim.c
SOURCES += $(NETCDFTOP)/libdispatch/datt.c
SOURCES += $(NETCDFTOP)/libdispatch/dattinq.c
SOURCES += $(NETCDFTOP)/libdispatch/dattput.c
SOURCES += $(NETCDFTOP)/libdispatch/dattget.c
SOURCES += $(NETCDFTOP)/libdispatch/derror.c
SOURCES += $(NETCDFTOP)/libdispatch/dvar.c
SOURCES += $(NETCDFTOP)/libdispatch/dvarget.c
SOURCES += $(NETCDFTOP)/libdispatch/dvarput.c
SOURCES += $(NETCDFTOP)/libdispatch/dvarinq.c
SOURCES += $(NETCDFTOP)/libdispatch/dinternal.c
SOURCES += $(NETCDFTOP)/libdispatch/ddispatch.c
SOURCES += $(NETCDFTOP)/libdispatch/dutf8.c
SOURCES += $(NETCDFTOP)/libdispatch/nclog.c
SOURCES += $(NETCDFTOP)/libdispatch/dstring.c
SOURCES += $(NETCDFTOP)/libdispatch/ncuri.c
SOURCES += $(NETCDFTOP)/libdispatch/nclist.c
SOURCES += $(NETCDFTOP)/libdispatch/ncbytes.c
SOURCES += $(NETCDFTOP)/libdispatch/nchashmap.c
SOURCES += $(NETCDFTOP)/libdispatch/nctime.c
SOURCES += $(NETCDFTOP)/libdispatch/nc.c
SOURCES += $(NETCDFTOP)/libdispatch/nclistmgr.c
SOURCES += $(NETCDFTOP)/libdispatch/drc.c
SOURCES += $(NETCDFTOP)/libdispatch/dauth.c
SOURCES += $(NETCDFTOP)/libdispatch/doffsets.c
SOURCES += $(NETCDFTOP)/libdispatch/dwinpath.c
SOURCES += $(NETCDFTOP)/libdispatch/dutil.c
SOURCES += $(NETCDFTOP)/libdispatch/utf8proc.c

SOURCES += $(NETCDFTOP)/liblib/nc_initialize.c
