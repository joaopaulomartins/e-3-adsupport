ZLIBTOP = $(APP)/zlibSrc

USR_INCLUDES += -I$(where_am_I)$(ZLIBTOP)
USR_INCLUDES += -I$(where_am_I)$(ZLIBTOP)/$(OS_Linux)
USR_INCLUDES += -I$(where_am_I)$(ZLIBTOP)/$(OS_default)

HEADERS += $(ZLIBTOP)/$(OS_default)/zlib.h
HEADERS += $(ZLIBTOP)/$(OS_Linux)/zconf.h

SOURCES += $(ZLIBTOP)/adler32.c
SOURCES += $(ZLIBTOP)/compress.c
SOURCES += $(ZLIBTOP)/crc32.c
SOURCES += $(ZLIBTOP)/deflate.c
SOURCES += $(ZLIBTOP)/gzclose.c
SOURCES += $(ZLIBTOP)/gzlib.c
SOURCES += $(ZLIBTOP)/gzread.c
SOURCES += $(ZLIBTOP)/gzwrite.c
SOURCES += $(ZLIBTOP)/infback.c
SOURCES += $(ZLIBTOP)/inffast.c
SOURCES += $(ZLIBTOP)/inflate.c
SOURCES += $(ZLIBTOP)/inftrees.c
SOURCES += $(ZLIBTOP)/trees.c
SOURCES += $(ZLIBTOP)/uncompr.c
SOURCES += $(ZLIBTOP)/zutil.c
