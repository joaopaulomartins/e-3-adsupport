SZIPTOP = $(APP)/szipSrc

USR_INCLUDES += -I$(where_am_I)$(SZIPTOP)/$(OS_default)

HEADERS += $(SZIPTOP)/$(OS_default)/SZconfig.h
HEADERS += $(SZIPTOP)/$(OS_default)/rice.h
HEADERS += $(SZIPTOP)/$(OS_default)/ricehdf.h
HEADERS += $(SZIPTOP)/$(OS_default)/szip_adpt.h
HEADERS += $(SZIPTOP)/$(OS_default)/szlib.h

SOURCES += $(SZIPTOP)/encoding.c
SOURCES += $(SZIPTOP)/rice.c
SOURCES += $(SZIPTOP)/sz_api.c

