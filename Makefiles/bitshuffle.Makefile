BITSHUFFLETOP = $(APP)/bitshuffleSrc
USR_CFLAGS_Linux += -std=c99
HEADERS += $(BITSHUFFLETOP)/bitshuffle.h
HEADERS += $(BITSHUFFLETOP)/bitshuffle_core.h

SOURCES += $(BITSHUFFLETOP)/bitshuffle.c
SOURCES += $(BITSHUFFLETOP)/bitshuffle_core.c
SOURCES += $(BITSHUFFLETOP)/iochain.c

HEADERS += $(BITSHUFFLETOP)/lz4/lz4.h
SOURCES += $(BITSHUFFLETOP)/lz4/lz4.c
